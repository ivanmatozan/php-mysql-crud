<?php
$pageTitle = 'Kase';
$navActive = 'popis';
include_once 'database.php';

// Briše kasu iz baze
if (isset($_GET['id'])) {
  $sql = "DELETE FROM kasa WHERE id = ?";
  $args = array($_GET['id']);
  Database::run($sql, $args);
}

// Include Header
include_once 'inc/header.php';
?>

<div class="popis-buttons">
  <a href="n_kasa.php" class="btn btn-primary" role="button">Dodaj Kasu</a>
</div>

<table class='table table-hover table-responsive table-bordered'>
  <tr>
    <th>Model</th>
    <th>Trgovina</th>
    <th>Partner</th>
  </tr>
  <?php
  // Dohvaća kase te trgovinu i partnera kojemu kasa pripada
  $sql = "SELECT kasa.id AS k_id, model, trgovina_id, trgovina.naziv AS t_naziv,
    partner.id AS p_id, partner.naziv AS p_naziv
    FROM kasa JOIN trgovina
    ON trgovina_id = trgovina.id
    JOIN partner
    ON partner_id = partner.id
    ORDER BY partner.id, trgovina_id";
  $stmt = Database::run($sql);

  // Ispisuje popis kasa
  foreach ($stmt as $row) {
    echo "<tr>";
    echo "<td>{$row[model]}</td>";
    echo "<td>{$row[t_naziv]}</td>";
    echo "<td>{$row[p_naziv]}</td>";

    // Uredi button
    echo "<td>";
    echo "<a href='u_kasa.php?id={$row[k_id]}' class='btn btn-warning left-margin'>";
    echo "<span class='glyphicon glyphicon-edit'></span> Uredi";
    echo "</a>";
    echo "</td>";

    // Obriši button
    echo "<td>";
    echo "<a href='p_kasa.php?id={$row[k_id]}' class='btn btn-danger delete'>";
    echo "<span class='glyphicon glyphicon-remove'></span> Obriši";
    echo "</a>";
    echo "</td>";

    echo "</tr>";
  }
  ?>
</table>

<?php include_once 'inc/footer.php'; ?>
