<?php
$pageTitle = 'Uredi Trgovinu';
$navActive = null;
include_once 'database.php';

// Za $_POST upisuje promjene u bazu, a za $_GET dohvaća podatke o odabranoj trgovini
if (isset($_POST['submit'])) {
  $sql = "UPDATE trgovina SET naziv = ?, ulica_br = ?, naselje = ?, partner_id = ? WHERE id = ?";
  $args = array($_POST['naziv'], $_POST['ulica_br'], $_POST['naselje'], $_POST['partner_id'], $_POST['id']);
  Database::run($sql, $args);

  header("location:p_trgovina.php");
}
elseif ($_GET['id']) {
  $sql = "SELECT * FROM trgovina WHERE id = ?";
  $args = array($_GET['id']);
  $stmt = Database::run($sql, $args)->fetch();
}

// Include Header
include_once 'inc/header.php';
?>

<form action='u_trgovina.php' role="form" method='post'>
  <table class='table table-hover table-responsive table-bordered'>
    <input type="hidden" name="id" value="<?php echo $_GET['id']; ?>">
    <tr>
      <td>Naziv</td>
      <td>
        <input type="text" name="naziv" class="form-control" placeholder="Naziv" value="<?php echo $stmt['naziv']; ?>" required>
      </td>
    </tr>
    <tr>
      <td>Ulica i broj</td>
      <td>
        <input type="text" name="ulica_br" class="form-control" placeholder="OIB" value="<?php echo $stmt['ulica_br']; ?>" required>
      </td>
    </tr>
    <tr>
      <td>Naselje</td>
      <td>
        <input type="text" name="naselje" class="form-control" placeholder="Ulica i broj" value="<?php echo $stmt['naselje']; ?>" required>
      </td>
    </tr>
    <tr>
      <td>Partner</td>
      <td>
        <select name="partner_id" class="form-control" required>

          <?php
          // Ispisuje podatke u formu i padajući izbornik
          $partneri = Database::run("SELECT * FROM partner");
          foreach ($partneri as $value) {
            echo "<option value=" . $value['id'];
            if ($value['id'] == $stmt['partner_id']) {
              echo " selected>";
            } else {
              echo ">";
            }
            echo $value['naziv'];
            echo "</option>";
          }
          ?>

        </select>
      </td>
    </tr>
    <tr>
      <td></td>
      <td>
        <button type="submit" name="submit" class="btn btn-primary">
          <span class="glyphicon glyphicon-plus"></span> Spremi
        </button>
      </td>
    </tr>
  </table>
</form>

<?php include_once 'inc/footer.php'; ?>
