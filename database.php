<?php
define('DB_HOST', 'localhost');
define('DB_NAME', 'zadatak');
define('DB_USERNAME', 'zadatak');
define('DB_PASSWORD', '1234');
define('DB_CHARSET', 'utf8');

class Database {
  protected static $instance;

  public function __construct() {}
  public function __clone() {}

  // Ako ne postoji stvara novi PDO objekt
  public static function connect() {
    if (self::$instance === null) {
      try {
        self::$instance = new PDO('mysql:host='.DB_HOST.';dbname='.DB_NAME.';charset='.DB_CHARSET, DB_USERNAME, DB_PASSWORD);
        self::$instance->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        self::$instance->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
      } catch (Exception $e) {
        echo "Error with Database: " . $e->getMessage();
      }
    }
    return self::$instance;
  }

  // Izvršava upite prema bazi
  public static function run($sql, $args = array()) {
    $stmt = self::connect()->prepare($sql);
    $stmt->execute($args);
    return $stmt;
  }
}
