<?php
$pageTitle = 'Nova Kasa';
$navActive = 'new';
include_once 'database.php';

// Upisuje kasu u bazu
if (isset($_POST['submit'])) {
  $sql = "INSERT INTO kasa (model, trgovina_id) VALUES (?, ?)";
  $args = array($_POST['model'], $_POST['trgovina_id']);
  Database::run($sql, $args);
}

// Include Header
include_once 'inc/header.php';
?>

<div class="popis-buttons">
  <a href="p_kasa.php" class="btn btn-primary" role="button">Kase</a>
</div>

<form action="n_kasa.php" role="form" method="post">
  <table class='table table-hover table-responsive table-bordered'>
    <tr>
      <td>Model</td>
      <td><input type="text" name="model"  class="form-control" placeholder="Model" required></td>
    </tr>
    <tr>
      <td>Trgovina</td>
      <td>
        <select name="trgovina_id" class="form-control" required>
          <option value="" disabled selected>Izaberi Trgovinu</option>

          <?php
          // Dohvaća trgovine iz baze i popunjava padajući izbornik
          $trgovine = Database::run("SELECT * FROM trgovina");
          foreach ($trgovine as $value) {
            echo "<option value=".$value['id'].">";
            echo $value['naziv'];
            echo "</option>";
          }
          ?>

        </select>
      </td>
    </tr>
    <tr>
      <td></td>
      <td>
        <button type="submit" name="submit" class="btn btn-primary">
          <span class="glyphicon glyphicon-plus"></span> Spremi
        </button>
      </td>
    </tr>
  </table>
</form>

<?php include_once 'inc/footer.php'; ?>
