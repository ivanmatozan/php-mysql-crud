<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title><?php echo $pageTitle; ?></title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="lib\css\bootstrap.min.css">
    <link rel="stylesheet" href="lib\css\bootstrap-theme.min.css">

    <!-- Main CSS -->
    <link rel="stylesheet" href="lib\css\main.css">
  </head>

  <body>
    <!-- container -->
    <div class="container">

      <div class="page-header">
        <h1><?php echo $pageTitle; ?></h1>
      </div>

      <!-- Navbar -->
      <nav class="navbar navbar-default">
        <div class="container-fluid">
          <ul class="nav navbar-nav">
            <li class="<?php if ($navActive == 'home') echo 'active' ?>"><a href="index.php">Home</a></li>
            <li class="dropdown <?php if ($navActive == 'popis') echo 'active' ?>">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Popis <span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="p_partner.php">Partneri</a></li>
                <li><a href="p_trgovina.php">Trgovine</a></li>
                <li><a href="p_kasa.php">Kase</a></li>
              </ul>
            </li>
            <li class="dropdown <?php if ($navActive == 'new') echo 'active' ?>">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Unesi <span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="n_partner.php">Novog Partner</a></li>
                <li><a href="n_trgovina.php">Novu Trgovinu</a></li>
                <li><a href="n_kasa.php">Novu Kasa</a></li>
              </ul>
            </li>
          </ul>
        </div><!-- /.container-fluid -->
      </nav>
