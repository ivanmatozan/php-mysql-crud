</div><!-- /container -->

<!-- Bootstrap core JS -->
<script src="lib\js\jquery-1.12.3.min.js"></script>
<script src="lib\js\bootstrap.min.js"></script>

<!-- jQuery delete confirmation -->
<script language="JavaScript" type="text/javascript">
$(document).ready(function(){
  $("a.delete").click(function(e){
    if(!confirm('Jeste li sigurni?')){
      e.preventDefault();
      return false;
    }
    return true;
  });
});
</script>

</body>
</html>
