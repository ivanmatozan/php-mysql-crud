# php-mysql-crud
PHP MySQL CRUD example

Opis:
Izmodelirati bazu podataka koja će služiti za pregled partnera, trgovina i kasa unutar pojedine trgovine, što znači da su premise sljedeće:
- N partnera
- N trgovina koje pripadaju pojedinom partneru
- N kasa koje pripadaju pojedinoj trgovini

Nakon modeliranja baze podataka potrebno je napraviti aplikaciju(WEB) koja će moći sljedeće:
- Unos/izmjena/brisanje novog partnera
- Unos/izmjena/brisanje nove trgovine
- Unos/izmjena/brisanje kase za pojedinu trgovinu
- Pregled trgovina i kasa po partneru

Tehnologije:
- DB: MySQL
- Aplikacija: HTML/JS, PHP
