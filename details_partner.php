<?php
$pageTitle = 'Detalji o Partneru';
$navActive = null;
include_once 'database.php';

// Ako je zatražen detaljan popis trgovina i kasa po partneru
if (isset($_GET['id'])) {
  $args = array($_GET['id']);

  // Dohvaca zatraženog partnera
  $sqlPartner = "SELECT naziv FROM partner WHERE id = ?";
  $partner = Database::run($sqlPartner, $args)->fetch();

  // Select trgovine po partneru
  $sqlTrgovine = "SELECT * FROM trgovina WHERE partner_id = ?";
  $trgovine = Database::run($sqlTrgovine, $args);

  // Select kase po trgovini
  $sqlKase = "SELECT * from kasa WHERE trgovina_id = ?";
} else {
  header("location:p_partner.php");
}

// Include Header
include_once 'inc/header.php';
?>

<div class="popis-buttons">
  <a href="n_partner.php" class="btn btn-primary" role="button">Novi Partner</a>
  <a href="n_trgovina.php" class="btn btn-primary" role="button">Nova Trgovina</a>
  <a href="n_kasa.php" class="btn btn-primary" role="button">Nova Kasa</a>
</div>

<h1><?php echo $partner['naziv']; ?></h1>

<ul>
  <?php
  echo "<h3><strong>TRGOVINE:</strong></h3>";

  // Ako partner posjeduje trgovine, ispiši ih
  if ($trgovine->rowCount() > 0) {
    foreach ($trgovine as $trgovina) {
      echo "<li><h3>" . $trgovina['naziv'] . "</h3></li>";

      $sqlKase = "SELECT * from kasa WHERE trgovina_id = ?";
      $kase = Database::run($sqlKase, array($trgovina['id']));

      echo "<ul>";
      echo "<h4><strong>KASE:</strong></h4>";

      // Ako trgovina posjeduje kase, ispiši ih
      if ($kase->rowCount() > 0) {
        foreach ($kase as $kasa) {
          echo "<li><h4>" . $kasa['model'] . "</h4></li>";
        }

      } else {
        echo "U ovoj trgovini nema kasa!";
      }
      echo "</ul>";
    }
  } else {
    echo "Ovaj partner ne posjeduje ni jednu trgovinu!";
  }

  ?>
</ul>

<?php include_once 'inc/footer.php'; ?>
