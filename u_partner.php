<?php
$pageTitle = 'Uredi Partnera';
$navActive = null;
include_once 'database.php';

// Za $_POST upisuje promjene u bazu, a za $_GET dohvaća podatke o odabranom partneru
if (isset($_POST['submit'])) {
  $sql = "UPDATE partner SET naziv = ?, oib = ?, ulica_br = ?, naselje = ? WHERE id = ?";
  $args = array($_POST['naziv'], $_POST['oib'], $_POST['ulica_br'], $_POST['naselje'], $_POST['id']);
  Database::run($sql, $args);

  header("location:p_partner.php");
}
elseif ($_GET['id']) {
  $sql = "SELECT * FROM partner WHERE id = ?";
  $args = array($_GET['id']);
  $stmt = Database::run($sql, $args)->fetch();
}

// Include Header
include_once 'inc/header.php';
?>

<form action='u_partner.php' role="form" method='post'>
  <table class='table table-hover table-responsive table-bordered'>
    <input type="hidden" name="id" value="<?php echo $_GET['id']; ?>">
    <tr>
      <td>Naziv</td>
      <td>
        <input type="text" name="naziv" class="form-control" placeholder="Naziv" value="<?php echo $stmt['naziv']; ?>" required>
      </td>
    </tr>
    <tr>
      <td>OIB</td>
      <td>
        <input type="text" name="oib" class="form-control" placeholder="OIB" value="<?php echo $stmt['oib']; ?>" required>
      </td>
    </tr>
    <tr>
      <td>Ulica i broj</td>
      <td>
        <input type="text" name="ulica_br" class="form-control" placeholder="Ulica i broj" value="<?php echo $stmt['ulica_br']; ?>" required>
      </td>
    </tr>
    <tr>
      <td>Naselje</td>
      <td>
        <input type="text" name="naselje" class="form-control" placeholder="Naselje" value="<?php echo $stmt['naselje']; ?>" required>
      </td>
    </tr>
    <tr>
      <td></td>
      <td>
        <button type="submit" name="submit" class="btn btn-primary">
          <span class="glyphicon glyphicon-plus"></span> Spremi
        </button>
      </td>
    </tr>
  </table>
</form>

<?php include_once 'inc/footer.php'; ?>
