<?php
$pageTitle = 'Trgovine';
$navActive = 'popis';
include_once 'database.php';

// Briše trgovinu i pripadajuče kase iz baze
if (isset($_GET['id'])) {
  $sql = "DELETE FROM trgovina WHERE id = ?";
  $args = array($_GET['id']);
  Database::run($sql, $args);
}

// Include Header
include_once 'inc/header.php';
?>

<div class="popis-buttons">
  <a href="n_trgovina.php" class="btn btn-primary" role="button">Dodaj Trgovinu</a>
</div>

<table class='table table-hover table-responsive table-bordered'>
  <tr>
    <th>Naziv</th>
    <th>Ulica i broj</th>
    <th>Naselje</th>
    <th>Partner</th>
  </tr>
  <?php
  // Dohvaća trgovine i partnere kojima pojedinda trgovina pripada
  $sql = "SELECT trgovina.id AS t_id, trgovina.naziv t_naziv, trgovina.ulica_br,
    trgovina.naselje, partner.naziv AS p_naziv, partner.id AS p_id
    FROM trgovina JOIN partner
    ON partner_id = partner.id
    ORDER BY partner_id";
  $stmt = Database::run($sql);

  // Ispisuje trgovine
  foreach ($stmt as $row) {
    echo "<tr>";
    echo "<td>{$row[t_naziv]}</td>";
    echo "<td>{$row[ulica_br]}</td>";
    echo "<td>{$row[naselje]}</td>";
    echo "<td>{$row[p_naziv]}</td>";

    // edit user button
    echo "<td>";
    echo "<a href='u_trgovina.php?id={$row[t_id]}' class='btn btn-warning left-margin'>";
    echo "<span class='glyphicon glyphicon-edit'></span> Uredi";
    echo "</a>";
    echo "</td>";

    // delete user button
    echo "<td>";
    echo "<a href='p_trgovina.php?id={$row[t_id]}' class='btn btn-danger delete'>";
    echo "<span class='glyphicon glyphicon-remove'></span> Obriši";
    echo "</a>";
    echo "</td>";

    echo "</tr>";
  }
  ?>
</table>

<?php include_once 'inc/footer.php'; ?>
