CREATE DATABASE IF NOT EXISTS zadatak
	CHARACTER SET utf8
  COLLATE utf8_unicode_ci;

USE zadatak;

CREATE TABLE partner (
	id INT PRIMARY KEY AUTO_INCREMENT,
  naziv VARCHAR(50),
  oib VARCHAR(11),
  ulica_br VARCHAR(75),
  naselje VARCHAR(20)
)
CHARACTER SET utf8
COLLATE utf8_unicode_ci
ENGINE=InnoDB;

CREATE TABLE trgovina (
	id INT PRIMARY KEY AUTO_INCREMENT,
  naziv VARCHAR(50),
  ulica_br VARCHAR(75),
  naselje VARCHAR(20),
  partner_id INT
)
CHARACTER SET utf8
COLLATE utf8_unicode_ci
ENGINE=InnoDB;

CREATE TABLE kasa (
	id INT PRIMARY KEY AUTO_INCREMENT,
  model VARCHAR(20),
  trgovina_id INT
)
CHARACTER SET utf8
COLLATE utf8_unicode_ci
ENGINE=InnoDB;

ALTER TABLE trgovina ADD FOREIGN KEY (partner_id) REFERENCES partner(id) ON DELETE CASCADE;
ALTER TABLE kasa ADD FOREIGN KEY (trgovina_id) REFERENCES trgovina(id) ON DELETE CASCADE;

INSERT INTO partner (id, naziv, oib, ulica_br, naselje) VALUES
	(1, 'BILLA d.o.o.', 72852689406, 'Riječka ulica 12', 'Zagreb'),
  (2, 'Boso d.o.o.', 48346325832, 'Ulica Hansa Dietricha Genschera 22B', 'Vinkovci'),
  (3, 'Plodine d.d.', 43946977329, 'Ružićeva 29', 'Rijeka'),
  (4, 'KONZUM d.d.', 80063606069, 'Marijana Čavića 1a', 'Zagreb'),
  (5, 'Lidl Hrvatska d.o.o', 16430997066, 'Kneza Ljudevita Posavskog 53', 'Velika Gorica'),
  (6, 'Pevec d.d.', 56748992902, 'Ante Trumbića 1b', 'Bjelovar');

	INSERT INTO trgovina (id, naziv, ulica_br, naselje, partner_id) VALUES
		(1, 'Billa MP1', 'Vinkovačka 13', 'Zagreb', 1),
		(2, 'Billa VP1', 'Zagrebačka 66', 'Vinkovci', 1),
		(3, 'Plodine MP1', 'Vukovarska 33', 'Osijek', 3),
		(4, 'Plodine MP2', 'Ulica 1', 'Županja', 3),
		(5, 'Plodine MP3', 'Koparska 6', 'Pula', 3);

	INSERT INTO kasa (id, model, trgovina_id) VALUES
		(1, "Kasa2000", 1),
		(2, "Kasa2001", 1),
		(3, "Kasa2003", 2),
		(4, "KS3000", 3),
		(5, "KS4000", 3),
		(6, "KS5000", 5);
