<?php
$pageTitle = 'Uredi Kasu';
$navActive = null;
include_once 'database.php';

// Za $_POST upisuje promjene u bazu, a za $_GET dohvaća podatke o odabranoj kasi
if (isset($_POST['submit'])) {
  $sql = "UPDATE kasa SET model = ?, trgovina_id = ? WHERE id = ?";
  $args = array($_POST['model'], $_POST['trgovina_id'], $_POST['id']);
  Database::run($sql, $args);

  header("location:p_kasa.php");
}
elseif ($_GET['id']) {
  $sql = "SELECT * FROM kasa WHERE id = ?";
  $args = array($_GET['id']);
  $stmt = Database::run($sql, $args)->fetch();
}

// Include Header
include_once 'inc/header.php';
?>

<form action="u_kasa.php" role="form" method="post">
  <table class='table table-hover table-responsive table-bordered'>
    <input type="hidden" name="id" value="<?php echo $_GET['id']; ?>">
    <tr>
      <td>Model</td>
      <td><input type="text" name="model"  class="form-control" placeholder="Model" value="<?php echo $stmt['model']; ?>" required></td>
    </tr>
    <tr>
      <td>Trgovina</td>
      <td>
        <select name="trgovina_id" class="form-control" required>

          <?php
          // Ispisuje podatke u formu i padajući izbornik
          $trgovine = Database::run("SELECT * FROM trgovina");
          foreach ($trgovine as $value) {
            echo "<option value=" . $value['id'];
            if ($value['id'] == $stmt['trgovina_id']) {
              echo " selected>";
            } else {
              echo ">";
            }
            echo $value['naziv'];
            echo "</option>";
          }
          ?>

        </select>
      </td>
    </tr>
    <tr>
      <td></td>
      <td>
        <button type="submit" name="submit" class="btn btn-primary">
          <span class="glyphicon glyphicon-plus"></span> Spremi
        </button>
      </td>
    </tr>
  </table>
</form>

<?php include_once 'inc/footer.php'; ?>
