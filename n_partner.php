<?php
$pageTitle = 'Novi Partner';
$navActive = 'new';
include_once 'database.php';

// Upisuje partnera u bazu
if (isset($_POST['submit'])) {
  $sql = "INSERT INTO partner (naziv, oib, ulica_br, naselje) VALUES (?, ?, ?, ?)";
  $args = array($_POST['naziv'], $_POST['oib'], $_POST['ulica_br'], $_POST['naselje']);
  Database::run($sql, $args);
}

// Include Header
include_once 'inc/header.php';
?>

<div class="popis-buttons">
  <a href="p_partner.php" class="btn btn-primary" role="button">Partneri</a>
</div>

<form action='n_partner.php' role="form" method='post'>
  <table class='table table-hover table-responsive table-bordered'>
    <tr>
      <td>Naziv</td>
      <td><input type='text' name='naziv'  class='form-control' placeholder="Naziv" required></td>
    </tr>
    <tr>
      <td>OIB</td>
      <td><input type='text' name='oib' class='form-control' placeholder="OIB" required></td>
    </tr>
    <tr>
      <td>Ulica i broj</td>
      <td><input type='text' name='ulica_br' class='form-control' placeholder="Ulica i broj" required></td>
    </tr>
    <tr>
      <td>Naselje</td>
      <td><input type='text' name='naselje' class='form-control' placeholder="Naselje" required></td>
    </tr>
    <tr>
      <td></td>
      <td>
        <button type="submit" name="submit" class="btn btn-primary">
          <span class="glyphicon glyphicon-plus"></span> Spremi
        </button>
      </td>
    </tr>
  </table>
</form>

<?php include_once 'inc/footer.php'; ?>
