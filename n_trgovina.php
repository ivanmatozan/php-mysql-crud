<?php
$pageTitle = 'Nova Trgovina';
$navActive = 'new';
include_once 'database.php';

// Upisuje partnera u bazu
if (isset($_POST['submit'])) {
  $sql = "INSERT INTO trgovina (naziv, ulica_br, naselje, partner_id) VALUES (?, ?, ?, ?)";
  $args = array($_POST['naziv'], $_POST['ulica_br'], $_POST['naselje'], $_POST['partner_id']);
  Database::run($sql, $args);
}

// Include Header
include_once 'inc/header.php';
?>

<div class="popis-buttons">
  <a href="p_trgovina.php" class="btn btn-primary" role="button">Trgovine</a>
</div>

<form action='n_trgovina.php' role="form" method='post'>
  <table class='table table-hover table-responsive table-bordered'>
    <tr>
      <td>Naziv</td>
      <td><input type='text' name='naziv'  class='form-control' placeholder="Naziv" required></td>
    </tr>
    <tr>
      <td>Ulica i broj</td>
      <td><input type='text' name='ulica_br' class='form-control' placeholder="Ulica i broj" required></td>
    </tr>
    <tr>
      <td>Naselje</td>
      <td><input type='text' name='naselje' class='form-control' placeholder="Mjesto" required></td>
    </tr>
    <tr>
      <td>Partner</td>
      <td>
        <select name="partner_id" class="form-control" required>
          <option value="" disabled selected>Izaberi Partnera</option>

          <?php
          // Dohvaća partnere iz baze i popunjava padajući izbornik
          $partneri = Database::run("SELECT * FROM partner");
          foreach ($partneri as $value) {
            echo "<option value=".$value['id'].">";
            echo $value['naziv'];
            echo "</option>";
          }
          ?>

        </select>
      </td>
    </tr>
    <tr>
      <td></td>
      <td>
        <button type="submit" name="submit" class="btn btn-primary">
          <span class="glyphicon glyphicon-plus"></span> Spremi
        </button>
      </td>
    </tr>
  </table>
</form>

<?php include_once 'inc/footer.php'; ?>
