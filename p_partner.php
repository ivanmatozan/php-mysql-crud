<?php
$pageTitle = 'Partneri';
$navActive = 'popis';
include_once 'database.php';

// Briše partnera te pripadajuće trgovine i kase iz baze
if (isset($_GET['id'])) {
  $sql = "DELETE FROM partner WHERE id = ?";
  $args = array($_GET['id']);
  Database::run($sql, $args);
}

// Include Header
include_once 'inc/header.php';
?>

<div class="popis-buttons">
  <a href="n_partner.php" class="btn btn-primary" role="button">Dodaj Partnera</a>
</div>

<table class='table table-hover table-responsive table-bordered'>
  <tr>
    <th>Naziv</th>
    <th>OIB</th>
    <th>Ulica i broj</th>
    <th>Naselje</th>
  </tr>
  <?php
  // Dohvaća partnere iz baze
  $sql = "SELECT * FROM partner";
  $stmt = Database::run($sql);

  // Ispisuje partnere
  foreach ($stmt as $row) {
    echo "<tr>";
    echo "<td><a href='details_partner.php?id={$row[id]}'>{$row[naziv]}</td>";
    echo "<td>{$row[oib]}</td>";
    echo "<td>{$row[ulica_br]}</td>";
    echo "<td>{$row[naselje]}</td>";

    // edit user button
    echo "<td>";
    echo "<a href='u_partner.php?id={$row[id]}' class='btn btn-warning left-margin'>";
    echo "<span class='glyphicon glyphicon-edit'></span> Uredi";
    echo "</a>";
    echo "</td>";


    // delete user button
    echo "<td>";
    echo "<a href='p_partner.php?id={$row[id]}' class='btn btn-danger delete'>";
    echo "<span class='glyphicon glyphicon-remove'></span> Obriši";
    echo "</a>";
    echo "</td>";

    echo "</tr>";
  }
  ?>
</table>

<?php include_once 'inc/footer.php'; ?>
