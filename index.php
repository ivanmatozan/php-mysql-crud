<?php
$pageTitle = 'Home';
$navActive = 'home';
include_once 'inc/header.php';
?>

<img class="home" src="img/home.png" alt="Home" />

<div class="new-buttons">
  <a href="n_partner.php" class="btn btn-primary" role="button">Novi Partner</a>
  <a href="n_trgovina.php" class="btn btn-primary" role="button">Nova Trgovina</a>
  <a href="n_kasa.php" class="btn btn-primary" role="button">Nova Kasa</a>
</div>


<?php include_once 'inc/footer.php'; ?>
